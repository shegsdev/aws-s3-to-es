import boto3
import json
import logging
from requests_aws4auth import AWS4Auth
import abc
import serializers
from storage.file import File


json.load_ = lambda bucket, f: bucket.Object(key=f).get()["Body"].read()


def get_all_s3_objects(s3, **base_kwargs):
  continuation_token = None
  while True:
    list_kwargs = dict(**base_kwargs)
    if continuation_token:
      list_kwargs['ContinuationToken'] = continuation_token
    response = s3.list_objects_v2(**list_kwargs)
    yield from response.get('Contents', [])
    if not response.get('IsTruncated'):
      break
    continuation_token = response.get('NextContinuationToken')


def get_last_key(key_storage):
  if key_storage['type'] == 'file':
    file_instance = File(key_storage['path'])
    try:
      last_key = file_instance.get()[-1]
    except IndexError:
      last_key = ''
    return last_key


def get_repo(repo, *args, **kwargs):
  if repo == 'AWS':
    return AWSRepo(kwargs['region'], kwargs['access_key'], kwargs['secret_access_key'])


def get_doc(type_, **kwargs):
  if type_ == 'ES':
    return {
      'name': kwargs['destination'],
      'host': kwargs['host'],
      'type': kwargs['type']
    }


def index_doc_to_es(data, bucket, obj, metadata):
  for data, key in data:
    last_key = key

    data_array = obj.transform(data)
    for doc in data_array:
      obj.load('S3', bucket, metadata, doc)
    return last_key


def save_last_key(key, path):
  file_instance = File(path)
  file_instance.save(key)


# Initialize Logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# Lambda execution
def handler(event, context):
  bucket = event['Records'][0]['s3']['bucket']['name']
  prefix = event['Records'][0]['s3']['bucket']['prefix']
  region = event['ctx']['region']
  host = event['ctx']['host']
  type_ = event['ctx']['type']
  destination = event['ctx']['destination']
  key_storage = event['config']['storage']
  access_key = event['config']['credentials']['key']
  secret_access_key = event['config']['credentials']['secret_key']

  repo = get_repo('AWS', bucket=bucket, region=region, access_key=access_key, secret_access_key=secret_access_key)

  last_key = get_last_key(key_storage)

  try:
    s3_data = repo.extract('S3', bucket, prefix=prefix, maxkeys=1000, startAfter=last_key)
    metadata = get_doc('ES', destination=destination, host=host, type=type_)
    last_key = index_doc_to_es(s3_data, bucket, repo, metadata)
    save_last_key(last_key, key_storage['path'])

  except Exception as e:
    logger.error(f'ERROR: {str(e)}')
    logger.error(f'ERROR: Unable able to GET object from S3 Bucket:{bucket}. Verify object exists.')


class ETLInterface(object, metaclass=abc.ABCMeta):
  @abc.abstractmethod
  def extract(self, source, location, *args, **kwargs):
    pass

  @abc.abstractmethod
  def transform(self, data):
    pass

  @abc.abstractmethod
  def load(self, source, location, destination, data):
    pass


class AWSRepo(ETLInterface):
  def __init__(self, region, access_key, secret_access_key):
    self.region = region
    self.access_key = access_key
    self.secret_access_key = secret_access_key

  def extract(self, source, location_name, *args, **kwargs):
    if source == 'S3':
      s3 = S3(location_name, self.region, self.access_key, self.secret_access_key)
      data = s3.load_data(kwargs['prefix'], kwargs['maxkeys'], kwargs['startAfter'])
      return data
    else:
      ValueError("Oops wrong source value")

  def transform(self, data_array):
    """
    :param data_array:
    :return:
    """
    data_main = []
    serializer = serializers.DataSerializer()
    for data in data_array:
      result = serializer.serialize(data, 'STR_ARR')
      data_main = data_main + result
    return data_main

  def load(self, source, location_name, destination, data):
    if source == 'S3':
      s3 = S3(location_name, self.region, self.access_key, self.secret_access_key)
      s3.save(data, destination)
    else:
      ValueError("Oops wrong source value")
    pass


class S3:
  def __init__(self, bucket, region, access_key, secret_access_key):
    self.bucket = bucket
    self.access_key = access_key
    self.secret_access_key = secret_access_key
    self.region = region
    self.s3 = boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_access_key)
    self.resource = boto3.resource('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_access_key)

  def load_data(self, prefix, maxkeys, startAfter):
    """
      Get Data from S3
    :param prefix:
    :param maxkeys:
    :param startAfter:
    :return: array of the data and the last element key
    """
    """Get data from S3 bucket."""
    data = []
    objects = get_all_s3_objects(self.s3, Bucket=self.bucket, MaxKeys=maxkeys, Prefix=prefix, StartAfter=startAfter)

    for obj in objects:
      last_key = obj['Key']
      data_string = json.load_(self.resource.Bucket(self.bucket), last_key).decode('utf-8')
      data.append(data_string)

      logger.info('SUCCESS: Successfully retrieved doc from S3')
      yield data, last_key

  def save(self, data, destination):
    """
      Store data
    :param data:
    :param destination:
    :return: boolean
    """

    auth = self.authorize()
    serializers.S3Serializer.save(data, destination, auth)

  def authorize(self):

    try:
      aws_auth = AWS4Auth(self.access_key, self.secret_access_key, self.region, 'es')
      return aws_auth
    except Exception as e:
      logger.error(f'Authorization failed: {e}')
      return False

