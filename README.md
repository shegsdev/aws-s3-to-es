## aws-s3-to-es

Boiler-plate for streaming data from s3 bucket to Elasticsearch

### Installation
To run the program, you need to have Python installed on your machine.
Follow this [link]("https://python.org") to download Python.

Next...
Clone this repo and move into the project directory. <br>

_NB_: _Make sure you have **virtualenv** installed._ Click [here](https://gist.github.com/frfahim/73c0fad6350332cef7a653bcd762f08d) to see how you can set it up.<br>
Open your `terminal` or `command prompt`  and run: <br>
```
pip install -r requirements.txt
```
to install all required packages. If everything works well, move on!

___

### Finally...let's run the program
Before you do,
* Open event.json and fill in the necessary credentials

In the command line, enter:
```
python-lambda-local -f handler -t 20000 index.py event.json
```