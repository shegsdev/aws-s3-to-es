import os


class File:

    def __init__(self, path):
        self._path = path
        if not os.path.exists(self._path):
            with open(self._path, 'w+', encoding='utf-8') as f:
                f.write('')

    def get(self):
        results = set()
        with open(os.getcwd() + self._path, 'rt') as f:
            for line in f:
                results.add(line.replace('\n', ''))
            return [*results]

    def save(self, data):
        with open(os.getcwd() + self._path, 'w', encoding='utf-8') as f:
            f.write(data + '\n')
