import json
import logging
import requests
import datetime

# Initialize Logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class DataSerializer:

    def serialize(self, data, format_):
        if format_ == "STR_ARR":
            return self.convert_str_arr_to_json(data)
        else:
            raise ValueError(format_)

    def convert_str_arr_to_json(self, string_arr):
        data = []
        data_array = string_arr.split('\n')
        for data_ in data_array:
            data_main = json.loads(data_)
            data.append(data_main)
        return data


class S3Serializer:

    @staticmethod
    def save(data, destination, auth):
        """Store data to Elastic search."""
        if destination['name'] == 'ES':
            index = f's3-to-es-{str(datetime.date.today().year)}'
            url = destination['host'] + '/' + index + '/' + destination['type']
            try:
                headers = {"Content-Type": "application/json"}
                response = requests.post(url, auth=auth, json=data, headers=headers)
                if response.status_code == 201:
                    logger.info('INFO: Successfully inserted element into ES')
                    return True
                else:
                    logger.error('FAILURE: Unable to index element')
            except Exception as e:
                logger.error(f'ERROR: {str(e)}')
                logger.error('ERROR: Unable to index line...')
                logger.error(e)
        else:
            raise ValueError(destination)
